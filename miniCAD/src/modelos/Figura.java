package modelos;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Rosario
 */
public class Figura {

    Color c = null;
    int grosor;

    void setColor(Color c) {
        this.c = c;
    }

    void setGrosor(int g) {
        this.grosor = g;
    }

    Color getColor() {
        return this.c;
    }

    public int getGrosor() {
        return this.grosor;
    }

    public void dibujar(Graphics2D g) {
        g.setStroke(new BasicStroke(grosor));
    }
}

package modelos;


import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rosario
 */
public class Rectangulo extends Figura{
    Point pA = null;
    Point pB = null;
    Point pC = null;
    int w, x,y,h;
 
    public Rectangulo(Point pA, Point pB){
        this.pA = pA;
        this.pB = pB;
    }
    
    public Rectangulo(Point pA, Point pB, Color c){
        this.pA = pA;
        this.pB = pB;
        
    }
    
    public Rectangulo(int x1, int y1, int x2, int y2 ){
        this.pA = new Point(x1,y1);
        this.pB = new Point(x2,y2);
       

    }    
    
    public Rectangulo(int x1, int y1, int x2, int y2 , Color c){
        this.pA = new Point(x1,y1);
        this.pB = new Point(x2,y2);
         
        
    }    
    
    Point getPointA(){
        return this.pA;
    }

    Point getPointB(){
        return this.pB;
    } 
        
    @Override
    public void dibujar(Graphics2D g2){ 
            super.dibujar(g2);
            int width =(int) (this.pA.getX() - this.pB.getX());
            int  height = (int)(this.pA.getY() - this.pB.getY());
             w = Math.abs( width );
             h = Math.abs( height );
             x = width < 0 ? (int)(pA.getX()) : (int)(pB.getX());            
             y = height < 0 ? (int)(pA.getY()) :(int)(pB.getY());
            g2.setStroke(new BasicStroke(grosor));
    Color tmp = g2.getColor();
        
        if (this.c!=null){
            g2.setColor(c);
        }
        
        g2.drawRect(x, y, w, h);//g.drawOval((int)pA.getX(), (int)pA.getY(), 
                    //(int)pB.getX(), (int)pB.getY());
        
        g2.setColor(tmp);
}
}

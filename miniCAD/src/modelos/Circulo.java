package modelos;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Rosario
 */
public class Circulo extends Figura {

    Point pA = null;
    Point pB = null;
    Color c = null;
    int grosor;

    public Circulo(Point pA, Point pB) {
        this.pA = pA;
        this.pB = pB;
    }

    public Circulo(Point pA, Point pB, Color c) {
        this.pA = pA;
        this.pB = pB;
        this.c = c;
    }

    public Circulo(int x1, int y1, int x2, int y2) {
        this.pA = new Point(x1, y1);
        this.pB = new Point(x2, y2);
    }

    public Circulo(int x1, int y1, int x2, int y2, Color c, int grosor) {
        this.pA = new Point(x1, y1);
        this.pB = new Point(x2, y2);
        this.c = c;
    }

    Point getPointA() {
        return this.pA;
    }

    Point getPointB() {
        return this.pB;
    }

    @Override
    public void dibujar(Graphics2D g) {
        super.dibujar(g);
        int width = (int) (this.pA.getX() - this.pB.getX());
        int height = (int) (this.pA.getY() - this.pB.getY());
        int w = Math.abs(width);
        int h = Math.abs(height);
        int x = width < 0 ? (int) (pA.getX()) : (int) (pB.getX());
        int y = height < 0 ? (int) (pA.getY()) : (int) (pB.getY());
        g.setStroke(new BasicStroke(grosor));
        
        
        
        Color tmp = g.getColor();
        
        if (this.c!=null){
            g.setColor(c);
        }
        
        g.drawOval(x, y, w, h);//g.drawOval((int)pA.getX(), (int)pA.getY(), 
                    //(int)pB.getX(), (int)pB.getY());
        
        g.setColor(tmp);
        
    }
    }

    


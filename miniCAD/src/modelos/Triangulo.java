package modelos;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Rosario
 */
public class Triangulo extends Figura {

    Point pA = null;
    Point pB = null;
    Point pC = null;

    public Triangulo(Point pA, Point pB, Point pC) {
        this.pA = pA;
        this.pB = pB;
        this.pC = pC;
    }

    public Triangulo(Point pA, Point pB, Point pC, Color c) {
        this.pA = pA;
        this.pB = pB;
        this.pC = pC;
    }

    public Triangulo(int x1, int y1, int x2, int y2, int x3, int y3) {
        this.pA = new Point(x1, y1);
        this.pB = new Point(x2, y2);
        this.pC = new Point(x3, y3);
    }

    public Triangulo(int x1, int y1, int x2, int y2, int x3, int y3, Color c) {
        this.pA = new Point(x1, y1);
        this.pB = new Point(x2, y2);
        this.pC = new Point(x3, y3);

    }

    Point getPointA() {
        return this.pA;
    }

    Point getPointB() {
        return this.pB;
    }

    Point getPointC() {
        return this.pC;
    }

    @Override
    public void dibujar(Graphics2D g) {
        super.dibujar(g);
        Graphics2D g2 = (Graphics2D) g;
        int a[] = {(int) pA.getX(), (int) pB.getX(), (int) pC.getX()};
        int b[] = {(int) pA.getY(), (int) pB.getY(), (int) pC.getY()};
        
        
        Color tmp = g.getColor();
        
        if (this.c!=null){
            g.setColor(c);
        }
        
        g.drawPolygon(a, b, 3);//g.drawOval((int)pA.getX(), (int)pA.getY(), 
                    //(int)pB.getX(), (int)pB.getY());
        
        g.setColor(tmp);
    }

}

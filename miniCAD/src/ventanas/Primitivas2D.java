package ventanas;

import modelos.FuenteVisual;
import modelos.Rectangulo;
import modelos.Circulo;
import modelos.Linea;
import modelos.Triangulo;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Iterator;
import modelos.Figura;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Rosario
 */
public class Primitivas2D extends javax.swing.JFrame {

    /**
     * Creates new form Primitivas2D
     */
    public final int LINEA = 1;
    public final int TRIANGULO = 2;
    public final int RECTANGULO = 3;
    public final int CIRCULO = 4;
    public final int TEXTO = 5;

    private Point puntoA = null;
    private Point puntoB = null;
    private Point puntoC = null;

    private int iFigura = this.LINEA;

    public Primitivas2D() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonCerrar = new javax.swing.JButton();
        labelEstado = new javax.swing.JLabel();
        buttonRectangulo = new javax.swing.JToggleButton();
        buttonCirculo = new javax.swing.JToggleButton();
        txtTexto = new javax.swing.JTextField();
        panelDibujo = new ventanas.PanelDibujo();
        buttonTriangulo = new javax.swing.JToggleButton();
        buttonLinea = new javax.swing.JToggleButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        buttonCerrar.setText("Cerrar");
        buttonCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCerrarActionPerformed(evt);
            }
        });

        buttonGroup1.add(buttonRectangulo);
        buttonRectangulo.setText("Rectángulo");
        buttonRectangulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonRectanguloActionPerformed(evt);
            }
        });

        buttonGroup1.add(buttonCirculo);
        buttonCirculo.setText("Círculo");
        buttonCirculo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCirculoActionPerformed(evt);
            }
        });

        txtTexto.setEnabled(false);

        panelDibujo.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                panelDibujoMouseMoved(evt);
            }
        });
        panelDibujo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                panelDibujoMousePressed(evt);
            }
        });

        javax.swing.GroupLayout panelDibujoLayout = new javax.swing.GroupLayout(panelDibujo);
        panelDibujo.setLayout(panelDibujoLayout);
        panelDibujoLayout.setHorizontalGroup(
            panelDibujoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 565, Short.MAX_VALUE)
        );
        panelDibujoLayout.setVerticalGroup(
            panelDibujoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 332, Short.MAX_VALUE)
        );

        buttonGroup1.add(buttonTriangulo);
        buttonTriangulo.setText("Triangulo");
        buttonTriangulo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonTrianguloActionPerformed(evt);
            }
        });

        buttonGroup1.add(buttonLinea);
        buttonLinea.setSelected(true);
        buttonLinea.setText("Línea");
        buttonLinea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonLineaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(labelEstado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(24, 24, 24))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(57, 57, 57)
                .addComponent(buttonLinea, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(52, 52, 52)
                .addComponent(buttonRectangulo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonCirculo, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(66, 66, 66)
                .addComponent(buttonTriangulo, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32))
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(panelDibujo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(17, 17, 17))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(txtTexto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(267, 267, 267)
                        .addComponent(buttonCerrar)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonTriangulo)
                    .addComponent(buttonLinea)
                    .addComponent(buttonRectangulo)
                    .addComponent(buttonCirculo))
                .addGap(18, 18, 18)
                .addComponent(panelDibujo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(txtTexto, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(buttonCerrar)
                .addGap(18, 18, 18)
                .addComponent(labelEstado)
                .addContainerGap(29, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCerrarActionPerformed
        // TODO add your handling code here:
        buttonCerrar.setBackground(Color.gray);
        System.exit(0);
    }//GEN-LAST:event_buttonCerrarActionPerformed

    private void buttonLineaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonLineaActionPerformed
        // TODO add your handling code here:
        buttonLinea.setBackground(Color.blue);
        if (this.iFigura != this.LINEA) {
            this.puntoA = null;
            this.puntoB = null;
            this.puntoC = null;
        }
        this.iFigura = this.LINEA;
        this.txtTexto.setEnabled(false);
    }//GEN-LAST:event_buttonLineaActionPerformed

    private void buttonTrianguloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonTrianguloActionPerformed
        // TODO add your handling code here:
        buttonTriangulo.setBackground(Color.yellow);
        if (this.iFigura != this.TRIANGULO) {
            this.puntoA = null;
            this.puntoB = null;
            this.puntoC = null;
        }
        this.iFigura = this.TRIANGULO;
        this.txtTexto.setEnabled(false);
    }//GEN-LAST:event_buttonTrianguloActionPerformed

    private void buttonRectanguloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonRectanguloActionPerformed
        // TODO add your handling code here:
        buttonRectangulo.setBackground(Color.red);
        if (this.iFigura != this.RECTANGULO) {
            this.puntoA = null;
            this.puntoB = null;
            this.puntoC = null;
        }
        this.iFigura = this.RECTANGULO;
        this.txtTexto.setEnabled(false);
    }//GEN-LAST:event_buttonRectanguloActionPerformed

    private void buttonCirculoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCirculoActionPerformed
        // TODO add your handling code here:
        buttonCirculo.setBackground(Color.pink);
        if (this.iFigura != this.CIRCULO) {
            this.puntoA = null;
            this.puntoB = null;
            this.puntoC = null;
        }
        this.iFigura = this.CIRCULO;
        this.txtTexto.setEnabled(false);
    }//GEN-LAST:event_buttonCirculoActionPerformed

    private void panelDibujoMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelDibujoMouseMoved
        String sAccion = "";
        String sPos = "x=" + evt.getX() + ", y=" + evt.getY();

        if (this.puntoA != null) {
            sAccion = "Punto Inicial (" + puntoA.getX() + "," + puntoA.getY() + ") ";
        }
        this.labelEstado.setText(sPos + " " + sAccion);
    }//GEN-LAST:event_panelDibujoMouseMoved

    private void panelDibujoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelDibujoMousePressed

        switch (this.iFigura) {
            case LINEA:
                if (puntoA == null) {
                    this.puntoA = evt.getPoint();
                } else {
                    this.puntoB = evt.getPoint();
                    Graphics2D g2 = (Graphics2D) this.panelDibujo.getGraphics();
                    Linea linea = new Linea(puntoA, puntoB, Color.blue);
                    linea.dibujar(g2);
                    panelDibujo.getFiguras().add(linea);
                    puntoA = null;
                    puntoB = null;
                }
                break;

            case TRIANGULO:
                if (puntoA == null) {
                    this.puntoA = evt.getPoint();
                } else if (puntoB == null) {
                    this.puntoB = evt.getPoint();
                } else {
                    this.puntoC = evt.getPoint();
                    Graphics2D g2 = (Graphics2D) this.panelDibujo.getGraphics();
                    Triangulo triangulo = new Triangulo(puntoA, puntoB, puntoC, Color.blue);
                    triangulo.dibujar(g2);
                    panelDibujo.getFiguras().add(triangulo);

                    puntoA = null;
                    puntoB = null;

                }
                break;
            case RECTANGULO:
                if (puntoA == null) {
                    this.puntoA = evt.getPoint();
                } else {
                    this.puntoB = evt.getPoint();

                    Graphics2D g2 = (Graphics2D) this.panelDibujo.getGraphics();

                    Rectangulo rectangulo = new Rectangulo(puntoA, puntoB, Color.red);

                    rectangulo.dibujar(g2);

                    panelDibujo.getFiguras().add(rectangulo);

                    puntoA = null;
                    puntoB = null;

                }
                break;
            case CIRCULO:
                if (puntoA == null) {
                    this.puntoA = evt.getPoint();
                } else {
                    this.puntoB = evt.getPoint();

                    Graphics2D g2 = (Graphics2D) this.panelDibujo.getGraphics();

                    Circulo circulo = new Circulo(puntoA, puntoB, Color.pink);

                    circulo.dibujar(g2);

                    panelDibujo.getFiguras().add(circulo);

                    puntoA = null;
                    puntoB = null;

                }
                break;
            case TEXTO:
                if (puntoA == null) {
                    this.puntoA = evt.getPoint();
                }
                Graphics2D g2 = (Graphics2D) this.panelDibujo.getGraphics();
                String cadena = txtTexto.getText();
                FuenteVisual texto = new FuenteVisual(puntoA, cadena);

                texto.dibujar(g2);

                panelDibujo.getFiguras().add(texto);

                puntoA = null;
                puntoB = null;
                break;
        }

        panelDibujo.repaint();
    }//GEN-LAST:event_panelDibujoMousePressed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Primitivas2D.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Primitivas2D.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Primitivas2D.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Primitivas2D.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Primitivas2D().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonCerrar;
    private javax.swing.JToggleButton buttonCirculo;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JToggleButton buttonLinea;
    private javax.swing.JToggleButton buttonRectangulo;
    private javax.swing.JToggleButton buttonTriangulo;
    private javax.swing.JLabel labelEstado;
    private ventanas.PanelDibujo panelDibujo;
    private javax.swing.JTextField txtTexto;
    // End of variables declaration//GEN-END:variables
}

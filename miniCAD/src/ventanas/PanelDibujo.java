/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ventanas;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import javax.swing.JPanel;
import modelos.Figura;

/**
 *
 * @author Rosario
 */
public class PanelDibujo extends JPanel{
    
    private ArrayList<Figura> figuras;
    
    public PanelDibujo(){
        this.figuras = new ArrayList();
        this.setBackground(Color.GREEN);
    }

    @Override
    protected void paintComponent(Graphics grphcs) {
        super.paintComponent(grphcs); //To change body of generated methods, choose Tools | Templates.
        dibujarFiguras(grphcs);
    }

    //Metodo para dibujar las figuras en el arreglo
    private void dibujarFiguras(Graphics g) {
        Graphics2D g2 = (Graphics2D)g;
        for (Figura figura : figuras) {
            figura.dibujar(g2);
        }
    }
    
    public ArrayList<Figura> getFiguras(){return figuras;}
    
    
}
